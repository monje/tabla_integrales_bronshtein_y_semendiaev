# Tools
LATEXMK = latexmk
RM = rm -f
RMFILES = *.bbl *.nav *.snm *.d *.aux *.fdb_latexmk *.fls *.log *.out *.toc .d

# Project-specific settings
LATEXFILE = tabla_integrales
INPUTTEXFILES = indefinidas.tex definidas.tex inmediatas.tex  prologo.tex
PDFNAME = $(LATEXFILE)
PDF = $(PDFNAME).pdf

# Targets
.PHONY: all clean

all: pdf

pdf: $(PDF) clean
$(PDFNAME).pdf: $(LATEXFILE).tex $(INPUTTEXFILES)
	$(LATEXMK) -jobname="$(PDFNAME)" -pdf -M -MP -MF $*.d $(LATEXFILE).tex

clean:
	@$(RM) $(RMFILES)

