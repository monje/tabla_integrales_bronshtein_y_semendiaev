# Tabla de integrales del Bronshtein y Semendiaev

Tabla de integrales indefinidas del libro "Manual de Matemáticas" de I. Bronshtein y K. Semendiaev (editorial Mir)

En mi opinión, la mejor, más completa y mejor organizada tabla de integrales indefinidas que conozco.

En LaTeX

En total son 515 integrales. Dado lo tedioso y lento del proceso, me lo tomo
como tarea secundaria. Mi objetivo es tenerlas pasadas todas para navidades del
2023.

